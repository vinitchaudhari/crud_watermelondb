/*Example of RealM Database in React Native*/
import React from 'react';

//Import react-navigation
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import HomeScreen from '../screens/HomeScreen';
import ViewAllUser from '../screens/ViewAllUser';
import UpdateUser from '../screens/UpdateUser';
import RegisterUser from '../screens/RegisterUser';


const Navigation = createStackNavigator({
    HomeScreen: {
        screen: HomeScreen,
        navigationOptions: {
            title: 'HomeScreen',
            headerStyle: { backgroundColor: '#3a59b7' },
            headerTintColor: '#ffffff',
        },
    },
    ViewAll: {
        screen: ViewAllUser,
        navigationOptions: {
            title: 'View All User',
            headerStyle: { backgroundColor: '#3a59b7' },
            headerTintColor: '#ffffff',
        },
    },
    Update: {
        screen: UpdateUser,
        navigationOptions: {
            title: 'Update User',
            headerStyle: { backgroundColor: '#3a59b7' },
            headerTintColor: '#ffffff',
        },
    },
    Register: {
        screen: RegisterUser,
        navigationOptions: {
            title: 'Register User',
            headerStyle: { backgroundColor: '#3a59b7' },
            headerTintColor: '#ffffff',
        },
    },
});
export default createAppContainer(Navigation);