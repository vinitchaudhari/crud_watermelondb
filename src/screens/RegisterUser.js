/*Screen to register the user*/
import React from 'react';
import { View, ScrollView, KeyboardAvoidingView, Alert, Button } from 'react-native';
import Mybutton from '../components/Mybutton';
import Mytextinput from '../components/Mytextinput';
import { database } from '../database';


class RegisterUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user_name: '',
            user_contact: '',
            user_address: '',
            user_image: ''
        };

    }

    alertfunc = () => {
        Alert.alert(
            'Success',
            'You are registered successfully',
            [
                {
                    text: 'Ok',
                    onPress: () => this.props.navigation.navigate('ViewAll'),
                },
            ],
            { cancelable: false }
        );
    }


    register_user = async () => {
        /*validate here */
        if (this.state.user_name) {
            if (this.state.user_contact) {
                if (this.state.user_address) {
                    await database.action(async () => {
                        const userDetail = database.collections.get('user_details');
                        return await userDetail.create((user) => {
                            user.user_name = this.state.user_name;
                            user.user_contact = this.state.user_contact;
                            user.user_address = this.state.user_address;
                        });
                    });
                    this.alertfunc();
                }
                else {
                    alert('Please fill Address');
                }
            }
            else {
                alert('Please fill Contact Number');
            }
        }
        else {
            alert('Please fill Name');
        }
    };

    render() {

        return (
            <View style={{ backgroundColor: 'white', flex: 1, marginTop: '50%' }}>
                <ScrollView keyboardShouldPersistTaps="handled">
                    <KeyboardAvoidingView
                        behavior="padding"
                        style={{ flex: 1, justifyContent: 'space-between' }}>
                        <Mytextinput
                            placeholder="Enter Name"
                            onChangeText={user_name => this.setState({ user_name })}
                        />
                        <Mytextinput
                            placeholder="Enter Contact No"
                            onChangeText={user_contact => this.setState({ user_contact })}
                            maxLength={10}
                            keyboardType="numeric"
                        />
                        <Mytextinput
                            placeholder="Enter Address"
                            onChangeText={user_address => this.setState({ user_address })}
                            maxLength={225}
                            numberOfLines={5}
                            multiline={true}
                            style={{ textAlignVertical: 'top' }}
                        />
                        <Mybutton
                            title="Submit"
                            customClick={this.register_user.bind(this)}
                        />
                    </KeyboardAvoidingView>
                </ScrollView>
            </View>
        );
    }
}
export default RegisterUser