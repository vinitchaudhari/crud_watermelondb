/*Screen to update the user*/
import React from 'react';
import {
    View,
    ScrollView,
    KeyboardAvoidingView,
    Alert,
} from 'react-native';
import Mybutton from '../components/Mybutton';
import Mytextinput from '../components/Mytextinput';
import { database } from '../database';

class UpdateUser extends React.Component {
    constructor(props) {
        super(props);
        const data = this.props.navigation.getParam("userData")
        this.state = {
            id: data.id,
            user_name: data.user_name,
            user_contact: data.user_contact,
            user_address: data.user_address,
        };
    }
    alertfunc = () => {
        Alert.alert(
            'Success',
            'User updated successfully',
            [
                {
                    text: 'Ok',
                    onPress: () =>
                        this.props.navigation.goBack(),
                },
            ],
            { cancelable: false }
        );
    }
    updateUser = async () => {
        if (this.state.user_name) {
            if (this.state.user_contact) {
                if (this.state.user_address) {
                    await database.action(async () => {
                        const userDetail = database.collections.get('user_details');
                        const post = await userDetail.find(this.state.id)
                        console.log("find post", post)
                        await post.update(user => {
                            user.user_name = this.state.user_name
                            user.user_contact = this.state.user_contact
                            user.user_address = this.state.user_address
                        })
                    }).catch(e => console.log(e))
                    this.alertfunc();
                }
                else {
                    alert('Please fill Address');
                }
            }
            else {
                alert('Please fill Contact Number');
            }
        } else {
            alert('Please fill Name');
        }
    };

    render() {
        return (
            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <ScrollView keyboardShouldPersistTaps="handled">
                    <KeyboardAvoidingView
                        behavior="padding"
                        style={{ flex: 1, justifyContent: 'space-between' }}>
                        {/* <Mytextinput
                            placeholder="Enter User Id"
                            onChangeText={input_user_id => this.setState({ input_user_id })}
                        /> */}
                        {/* <Mybutton
                            title="Search User"
                            customClick={this.searchUser.bind(this)}
                        /> */}
                        <Mytextinput
                            placeholder="Enter Name"
                            value={this.state.user_name}
                            onChangeText={user_name => this.setState({ user_name })}
                        />
                        <Mytextinput
                            placeholder="Enter Contact No"
                            value={'' + this.state.user_contact}
                            onChangeText={user_contact => this.setState({ user_contact })}
                            maxLength={10}
                            keyboardType="numeric"
                        />
                        <Mytextinput
                            value={this.state.user_address}
                            placeholder="Enter Address"
                            onChangeText={user_address => this.setState({ user_address })}
                            maxLength={225}
                            numberOfLines={5}
                            multiline={true}
                            style={{ textAlignVertical: 'top' }}
                        />
                        <Mybutton
                            title="Update User"
                            customClick={this.updateUser.bind(this)}
                        />
                    </KeyboardAvoidingView>
                </ScrollView>
            </View>
        );
    }
}
export default UpdateUser