/*Home Screen With buttons to navigate to diffrent options*/
import React from 'react';
import { StyleSheet, View } from 'react-native';
import Mybutton from '../components/Mybutton'

export default class HomeScreen extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.button}>
                <Mybutton
                    title="View List"
                    customClick={() => this.props.navigation.navigate('ViewAll')}
                />
            </View>
        );
    }
}
const styles = StyleSheet.create({
    button: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'white',
        flexDirection: 'column',
    },
})