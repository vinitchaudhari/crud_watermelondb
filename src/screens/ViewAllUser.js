/*Screen to view all the user*/
import withObservables from '@nozbe/with-observables';
import { withDatabase } from '@nozbe/watermelondb/DatabaseProvider';
import React from 'react';
import { Alert, Button, FlatList, Text, View } from 'react-native';
import Mybutton from '../components/Mybutton';
import { database } from '../database';

class ViewAllUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            FlatListItems: props.allUser,
        }
    }

    componentDidMount() {
        console.log("mounted")
        this.willFocusSubscription = this.props.navigation.addListener(
            'willFocus',
            () => {
                console.log("Refrsh")
                this.setState({ FlatListItems: this.props.allUser })
            }
        );
    }
    componentWillUnmount() {
        // Remove the event listener
        this.willFocusSubscription.remove();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.allUser !== this.props.allUser) {
            this.setState({ FlatListItems: this.props.allUser })
        }
        console.log("alluser ", this.props.allUser)
    }

    alertfunc = () => {
        Alert.alert(
            'Success',
            'User deleted successfully',
            [
                {
                    text: 'Ok',
                    onPress: () => this.props.navigation.navigate('ViewAll'),
                },
            ],
            { cancelable: false }
        );
    }

    deleteEntry = async (id) => {
        return await database.action(async () => {
            const userDetail = database.collections.get('user_details');
            const user = await userDetail.find(id)
            await user.markAsDeleted() // syncable
            await user.destroyPermanently() // permanent
        });
    }
    viewImage = (id) => {
        // this.props.viewImageAction(id);
    }
    ListViewItemSeparator = () => {
        return (
            <View style={{ height: 5, width: '100%', backgroundColor: '#f2f2f2' }} />
        );
    };

    renderList = (item) => {
        return (
            <View>
                <View style={{ backgroundColor: 'white', padding: 20, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View>
                        {/* <Text>Id: {item.user_id}</Text> */}
                        <Text>Name: {item.user_name}</Text>
                        <Text>Contact: {item.user_contact}</Text>
                        <Text>Address: {item.user_address}</Text>
                    </View>
                    <View style={{ justifyContent: "space-between", flexDirection: "row", alignItems: 'center' }}>
                        <Button
                            title="Edit"
                            color="#841584"
                            onPress={() => this.props.navigation.navigate('Update', {
                                userData: item
                            })}
                        />
                        <Button
                            title="Delete"
                            color="red"
                            onPress={() => this.deleteEntry(item.id)}
                        />
                    </View>
                </View>
            </View>
        )
    }

    render() {
        console.log("flatlistlisitem", this.state.FlatListItems)
        return (
            <View>
                <Mybutton
                    title="Add User"
                    customClick={() => this.props.navigation.navigate('Register')}
                />
                <View style={{ marginBottom: 100 }}>
                    <FlatList
                        contentContainerStyle={{ paddingHorizontal: 10, paddingBottom: 20 }}
                        data={this.state.FlatListItems}
                        ItemSeparatorComponent={this.ListViewItemSeparator}
                        keyExtractor={(item, index) => item.id}
                        // keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item }) => this.renderList(item)}
                    />
                </View>
            </View>
        );
    }
}

const enhance = withObservables(['user_details'], ({ database }) => ({
    allUser: database.collections.get('user_details').query().observe(),
}),
);
export default withDatabase(enhance(ViewAllUser));