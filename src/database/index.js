import SQLiteAdapter from "@nozbe/watermelondb/adapters/sqlite";
import Database from "@nozbe/watermelondb/Database";
import models from './model';
import schema from './model/schema'


export const adapter = new SQLiteAdapter({
    dbName: "usersList",
    schema
});
export const database = new Database({
    adapter,
    modelClasses: models,
    actionsEnabled: true,
});