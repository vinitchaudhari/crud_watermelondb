// model/schema.js
import { appSchema, tableSchema } from '@nozbe/watermelondb'

export default appSchema({
    version: 1,
    tables: [
        tableSchema({
            name: 'user_details',
            columns: [
                { name: 'user_id', type: 'number' },
                { name: 'user_name', type: 'string' },
                { name: 'user_contact', type: 'string' },
                { name: 'user_address', type: 'string' },
            ]
        }),
    ]
})


// export const realmpath = new Realm({ path: 'UserDatabase.realm' });