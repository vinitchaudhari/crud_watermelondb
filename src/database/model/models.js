import Model from "@nozbe/watermelondb/Model"
import { field, action } from '@nozbe/watermelondb/decorators';

export class USERS extends Model {
    static table = 'user_details'
    @field('user_name') user_name;
    @field('user_contact') user_contact;
    @field('user_address') user_address;
}