import React from 'react'
import { database } from './src/database'
import Navigation from './src/navigation/Navigation'
import DatabaseProvider from '@nozbe/watermelondb/DatabaseProvider';

function App() {
  return (
    <DatabaseProvider database={database}>
      <Navigation database={database} />
    </DatabaseProvider>
  )
}

export default App
